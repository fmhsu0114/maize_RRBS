< Maize RRBS pipeline >

Reduced representation bisulfite sequencing (RRBS) is a cost-effective method 
to acquire DNA methylation information from specifc genomic regions. 
Here we provide a pipeline for in silico digestion to identify methylation 
sensitive restriction enzymes (MSREs) whose fragments enriched in regions 
of interest (ROIs), such as promoter and genebody. We demonstrated this method on a maize epigenome analysis.


![pipeline](/uploads/55b31bb4f1fdebf16462f7a2beaf6af6/pipeline.png)


--Files needed to start--
* Reference genome FASTA (genome.fa)
* Gene annotation (refFlat.txt)
* Repeat annotation (repeats.gff3)
* MSRE and their cutting sites (enzyme_cutting_sites.txt)

-- System requirements--
* Linux/Unix or Mac OS
* python 2.7+
* R 3.2.0

(1) Build genome database

script: Build_Gnome.py  
input: reference genome FASTA file  
output: genome.shelve, log-genome.txt

```$ python Build_Gnome.py genome.fa```

(2) Build gene annotation database

script: Build_refgene_shelve.py  
input: gene annotation, log-genome.txt  
output: refgenes.shelve

```$ python Build_refgene_shelve.py refFlat.txt log-genome.txt```

(3) Build repeat annotation database

script: Build_repeats_Gnome.py  
input: repeats.gff3, log-genome.txt  
output: repeats.shelve

```$ python Build_repeats_Gnome.py repeats.gff3, log-genome.txt```

(4) Build maize genome CG, CHG. CHH sites database

script: Build_CG_CHG_CHH_Gnome.py  
input: genome.shelve, log-genome.txt  
output: CG-CHG-CHH.shelve

```$ python Build_CG_CHG_CHH_Gnome.py genome.shelve log-genome.txt```

(5) in silico MSRE digestion

script: Enzyme_digestion.py  
input: enzyme_cutting_sites.txt, genome.shelve, CG-CHG-CHH.shelve  
output: fragment_info (per MSRE and per size range), Summary-enzyme-coverage.txt

```$ python Enzyme_digestione.py```

"""" Move all fragment_info to ./fragment/ """"

(6) Computing RRBS fragments enrichment in genome features (per MSRE and per size range)

script: RRBS_fragments_enrichment.py  
input: repeats.shelve, refgenes.shelve  
output: log2_fold_enrichment (per MSRE and per size range)

```$ python RRBS_fragments_enrichment.py repeats.shelve, refgenes.shelve```

"""" Move all log2_fold_enrichment ./output/ """"

(7) Plotting enrichment barcharts

script: Barchart_enrichment.R  
input: enzyme_cutting_sites.txt, log2_fold_enrichment (per MSRE and per size range)  
output: ./png/Log2-Enrichment.png

```$ Rscript Barchart_enrichment.R```

(8) Computing fold enrichment for genebody and promoter

script: RRBS_fragments_enrichment_gene_level_per_MRE_Size.py  
input: refFlat.txt, CG-CHG-CHH.shelve, refgenes.shelve  
output: log2_fold_enrichment (per MSRE and per size range) of in silco RRBS fragments in genebody and promoter

```$ python RRBS_fragments_enrichment_gene_level_per_MRE_Size.py -i Enzyme -s size_range -c 1```
