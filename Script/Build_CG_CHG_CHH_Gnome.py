﻿import fileinput, string,os, operator, shelve, time, subprocess
from subprocess import Popen
from optparse import OptionParser
#---------------------------------------------------------------

def reverse_compl_seq(strseq):
	strseq=strseq.upper()
	rc_strseq=strseq.translate(string.maketrans("ATCG", "TAGC"))[::-1]
	return rc_strseq;
	
def C_tell_context(w):
	ctx="na"
	y1=w[0]
	y2=w[1]
	if y1=="G":
		ctx="CG"
	elif y1 in ["A","C","T"]:
		if y2=="G":
			ctx="CHG"
		elif y2 in ["A","C","T"]:
			ctx="CHH"
	return ctx;
def G_tell_context(w):
	ctx="na"
	y1=w[1]
	y2=w[0]
	if y1=="C":
		ctx="GC"
	elif y1 in ["A","G","T"]:
		if y2=="C":
			ctx="GHC"
		elif y2 in ["A","G","T"]:
			ctx="HHG"
	return ctx;
#---------------------------------------------------------------
C_map_table={'CG': "X", 'CHG': "Y", 'CHH': "Z","na":"-"}
G_map_table={'GC': "x", 'GHC': "y", 'HHG': "z","na":"-"}

gfile="log-maize_AGPv3-ref-genome.shelve.txt"
chr_length={}
for line in fileinput.input(gfile):
	l=line.split()
	if l[0]=="reference":
		chr=str(l[2])
		length=int(l[-2])
		chr_length[chr]=length
fileinput.close()

#---------------------------------------------------------------
ingenome_path="./"
ingenome_file='maize_AGPv3-ref-genome.shelve'

ind = shelve.open(ingenome_path+ingenome_file,'r')

genome_path="./"

genome_file="maize_AGPv3-CG-CHG-CHH.shelve"
d = shelve.open(genome_path+genome_file,'n')
logoutf=open("log-"+genome_file+".txt","w")

genome_seqs={}

chrs=ind.keys()
chrs.sort()

no_CG=0
no_CHG=0
no_CHH=0



for chr in chrs:
	print "ref seq: %s (%12d bp)"%(chr,len(ind[chr]));
	seq=ind[chr]
	seq=seq.upper()
	L=len(seq)
	newseq="--"
	i=2
	while i<L-2:
		if seq[i]=="C":
			word=seq[i+1:i+3]
			context=C_tell_context(word)
			newseq+=C_map_table[context]
		elif seq[i]=="G": 
			word=seq[i-2:i]
			context=G_tell_context(word)
			newseq+=G_map_table[context]
		else:
			newseq+="-"
		i+=1
	newseq+="--"
	no_CG+=newseq.count("X")+newseq.count("x")
	no_CHG+=newseq.count("Y")+newseq.count("y")
	no_CHH+=newseq.count("Z")+newseq.count("z")
	d[chr]=newseq
	logoutf.write("ref seq: %s (%12d bp)"%(chr,len(d[chr]))+"\n")
	#print "Newseq %s %d %s"%(chr,len(newseq),newseq[100000:100050]);
d.close()			
ind.close()

logoutf.write("CG sites: %d"%(no_CG)+"\n")
logoutf.write("CHG sites: %d"%(no_CHG)+"\n")
logoutf.write("CHH sites: %d"%(no_CHH)+"\n")

logoutf.close()
