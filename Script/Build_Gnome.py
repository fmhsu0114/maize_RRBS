﻿import fileinput, string,os, operator, shelve, time, subprocess
from subprocess import Popen
from optparse import OptionParser

#---------------------------------------------------------------
ingenome_path="./"
ingenome_file='maize_Ensembl_AGPv3_genome.fa'

#---------------------------------------------------------------
genome_path="./"

genome_file="maize_AGPv3-ref-genome.shelve"
d = shelve.open(genome_path+genome_file,'n')
logoutf=open("log-"+genome_file+".txt","w")

g=""
header=""

all_base=0
n=0

for line in fileinput.input(ingenome_path+ingenome_file):
	l=line.split()
	if line[0]!=">":
		g=g+line[:-1]
	elif line[0]==">":
		if header=="":
			n+=1
			header=l[0][1:]
			short_header=header
		else:
			g=g.upper()
			logoutf.write("reference seq: %s (renamed as %s ) %d bp"%(header,short_header,len(g))+"\n")
			d[short_header]=g
			all_base+=len(g)
			header=l[0][1:]
			n+=1
			short_header=header

			g=""
fileinput.close()
g=g.upper()
logoutf.write("reference seq: %s (renamed as %s ) %d bp"%(header,short_header,len(g))+"\n")
d[short_header]=g
all_base+=len(g)
logoutf.write("--- In tatal %d reference seqs ==> %d bp"%(len(d),all_base)+"\n")
d.close()
logoutf.close()
