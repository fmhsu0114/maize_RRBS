﻿import fileinput, string,os, operator, shelve, time, subprocess
from subprocess import Popen

#---------------------------------------------------------------
gfile="log-maize_AGPv3-ref-genome.shelve.txt"
chr_length={}
for line in fileinput.input(gfile):
	l=line.split()
	chr=str(l[2])
	length=int(l[-2])
	chr_length[chr]=length
fileinput.close()
chrs=chr_length.keys()
chrs.sort()

#---------------------------------------------------------------
genome_path="./"
genome_file="maize_AGPv3_enhanced_refgenes.shelve"
d = shelve.open(genome_path+genome_file,'n')

# Promoters "P","p"
# Exons "E","e"
# Introns "I","i"
# Intergentic "-"
# Splice site "S"
# UTR "5","3"
#---------------------------------------------------------------
file="uniq-38653-maize_Ensembl_AGPv3_refFlat.txt"

i=0
newseq=""
mychr0=""
for line in fileinput.input("./"+file):
	l=line.split()
	mychr=str(l[0])
	if "_" not in mychr:
		#-------------------------------------------------
		if mychr != mychr0 :
			if mychr0 !="":
				newseq_string="".join(newseq)
				d[mychr0]=newseq_string
				print mychr0,len(newseq_string);
				newseq=[]
			mychr0=mychr
			i=0
			L=chr_length[mychr]
			newseq=["-" for x in range(L)]
			
		#-- Genes and promoters ----------
		strand=l[3]
		tx1=int(l[4])
		tx2=int(l[5])
		cdx1=int(l[6])
		cdx2=int(l[7])
		gene=[tx1,tx2]
		if strand=="+":	
			TSS=min(tx1,tx2)
			pmt=[TSS-1000, TSS]
		else:
			TSS=max(tx1,tx2)
			pmt=[TSS, TSS+1000]
		#-- UTR --------------------------
		if strand=="+":	
			five=[tx1,cdx1]
			three=[cdx2,tx2]
		else:
			three=[tx1,cdx1]
			five=[cdx2,tx2]
			
		#-- Exons --------------------------
		no_exons=int(l[8])
		Exon_starts = str(l[9]).rstrip(",").split(",")
		Exon_ends   = str(l[10]).rstrip(",").split(",")
		exons=[]
		for i in range(no_exons):
			exons.append([int(Exon_starts[i]),int(Exon_ends[i])])
		exons.sort()
		#-- splice --------------------------
		splice_sites=[]
		if len(exons)>=2:
			for i in range(len(exons)-1):
				s1=exons[i][1]+1
				s2=exons[i+1][0]-1
				splice_sites.append(s1)
				splice_sites.append(s2)
				
			
			
		#--------------------------------------
		if strand=="+":
			for p in range(pmt[0],pmt[1]):
				newseq[p]="P"
			for p in range(tx1,tx2):
				newseq[p]="I"
			for exon in exons:
				for p in range(exon[0],exon[1]):
					newseq[p]="E"
		elif strand=="-":
			for p in range(pmt[0],pmt[1]):
				newseq[p]="p"
			for p in range(tx1,tx2):
				newseq[p]="i"
			for exon in exons:
				for p in range(exon[0],exon[1]):
					newseq[p]="e"
		if len(five)>0:
			for p in range(five[0],five[1]):
				newseq[p]="5"
		if len(three)>0:
			for p in range(three[0],three[1]):
				newseq[p]="3"
		for p in splice_sites:
			newseq[p]="S"
			

#-------------		
newseq_string="".join(newseq)
d[mychr0]=newseq_string
print mychr0,len(newseq_string);
'''
for c in chrs:
	if c not in d:
		d[c]="".join(["-" for x in range(chr_length[c])])
		print c,len(d[c]);

print len(d.keys()),d.keys();
'''
print "== END enhanced refgenes ==";
d.close()			

