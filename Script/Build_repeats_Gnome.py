﻿import fileinput, string,os, operator, shelve, time, subprocess
from subprocess import Popen

#---------------------------------------------------------------
gfile="log-maize_AGPv3-ref-genome.shelve.txt"
chr_length={}
for line in fileinput.input(gfile):
	l=line.split()
	if l[0]=="reference":
		chr=str(l[2])
		length=int(l[-2])
		chr_length[chr]=length
fileinput.close()
chrs=chr_length.keys()
chrs.sort()

#---------------------------------------------------------------
genome_file="maize_AGPv3_repeats.shelve"
d = shelve.open(genome_file,'n')

#---------------------------------------------------------------
file="repeats-Zea_mays.AGPv3.22.gff3"

i=0
newseq=""
mychr0=""
for line in fileinput.input("./"+file):
	l=line.split()
	mychr=str(l[0])
	if mychr in chrs:
		#-------------
		if mychr != mychr0 :
			if mychr0 !="":
				while i<L:
					newseq+="-"
					i+=1
				d[mychr0]=newseq
				print "Newseq %s %d %d %s"%(mychr0,L,len(newseq),newseq[100000:100030]);
			mychr0=mychr
			i=0
			newseq=""
			L=chr_length[mychr]
		#-------------
		p1=int(l[2])
		p2=int(l[3])
				
		while i<p1:
			newseq+="-"
			i+=1
		newseq+="-"
		i+=1
		
		while i>=p1 and i<p2:
			newseq+="I"
			i+=1
		newseq+="I"
		i+=1
#-------------		
while i<L:
	newseq+="-"
	i+=1
d[mychr0]=newseq
print "Newseq %s %d %d %s"%(mychr0,L,len(newseq),newseq[100000:100030]);
fileinput.close()

for c in chrs:
	if c not in d:
		L=chr_length[c]
		d[c]="".join(["-" for x in range(L)])
		print "Newseq %s %d %d"%(c,L,len(d[c]));
print "== END repeats ==";
d.close()			

