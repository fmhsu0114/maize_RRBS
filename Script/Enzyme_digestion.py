﻿import fileinput, string,os, operator, shelve, time, subprocess
from subprocess import Popen
import operator
from optparse import OptionParser
parser = OptionParser()

#parser.add_option("-u", "--upper", type="int",default=350,dest="up_info", help="upper_bound:350, 300", metavar="SAMP1")
#parser.add_option("-l", "--lower", type="int",default=50,dest="low_info", help="lower__bound:50, 100", metavar="SAMP2")

#(options, args) = parser.parse_args()
                  
#up=options.up_info
#low=options.low_info
#---------------------------------------------------------------
enzyme_file="enzyme_cutting_sites.txt"
enzyme_lst=[]

for line in fileinput.input(enzyme_file):
	if line[0]!="#":
		l=line.split()
		enzyme_lst.append([l[0],l[1]])
fileinput.close()



#ez="MseI"
#tag="TTAA"
#---------------------------------------------------------------
ingenome_path="../shelve/"
ingenome_file1="maize_AGPv3-ref-genome.shelve" #<-- +CG=X, -CG=x, +CHG=Y, -CHG=y, +CHH=Z, -CHH=z
ingenome_file2="maize_AGPv3-CG-CHG-CHH.shelve" #<-- +CG=X, -CG=x, +CHG=Y, -CHG=y, +CHH=Z, -CHH=z
ind = shelve.open(ingenome_path+ingenome_file1,'r')
indxyz = shelve.open(ingenome_path+ingenome_file2,'r')
chrs=ind.keys()
chrs.sort()
#---------------------------------------------------------------
nx=0
ny=0
nz=0
for c in chrs:
	seq=indxyz[c].upper()
	nx+=seq.count("X")
	ny+=seq.count("Y")
	nz+=seq.count("Z")
#---------------------------------------------------------------
outfile2="Summary-maize-AGPv3-enzyme-coverage.txt"
outf2=open(outfile2,"w")

for xy_enzyme in enzyme_lst:
	ez=xy_enzyme[0]
	tag=xy_enzyme[1]
	for up in [280,300]:
		for low in [40]:
			print ez,tag,low,up;
			outfile1="Fragments_"+ez+"_"+tag+"_"+str(low)+"-"+str(up)+".txt"
			outf1=open(outfile1,"w")
			n=0
			n_genome=0
			n_RRBS=0
			n_x=0
			n_y=0
			n_z=0

			for c in chrs:
				seq=ind[c].upper()
				xyzseq=indxyz[c].upper()
				L=len(seq)
				n_genome+=L

				i1=seq.find(tag)
				i2=seq.find(tag,i1+1,L)
				while i2>0:
					x2=i2+len(tag)
	
					d=x2-i1
					if d >=low and d<=up:
						n+=1
						n_RRBS+=d
						myseq=xyzseq[i1:x2]
						n_x+=myseq.count("X")
						n_y+=myseq.count("Y")
						n_z+=myseq.count("Z")
						outf1.write("%s\t%d\t%d\t%d"%(c,i1,x2,d)+"\n")
					i1=i2
					i2=seq.find(tag,i1+1,L)

			outf2.write("%s\t%s\t%d_%d\t%d\t%d\t%d\t%1.4f\t%d\t%d\t%1.4f\t%d\t%d\t%1.4f\t%d\t%d\t%1.4f"\
			%(ez,tag,low,up,n,n_genome,n_RRBS,float(n_RRBS)/n_genome,nx,n_x,float(n_x)/nx,ny,n_y,float(n_y)/ny,nz,n_z,float(n_z)/nz)+"\n")
			print "%s\t%s\t%d_%d\t%d\t%d\t%d\t%1.4f\t%d\t%d\t%1.4f\t%d\t%d\t%1.4f\t%d\t%d\t%1.4f"\
			%(ez,tag,low,up,n,n_genome,n_RRBS,float(n_RRBS)/n_genome,nx,n_x,float(n_x)/nx,ny,n_y,float(n_y)/ny,nz,n_z,float(n_z)/nz);
ind.close()			
indxyz.close()			
outf2.close()
