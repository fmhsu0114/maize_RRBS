﻿from os import listdir
import fileinput, string,os, operator, shelve, time, subprocess, math
from subprocess import Popen
import operator
from optparse import OptionParser
#parser = OptionParser()

#parser.add_option("-f", "--file", type="string",dest="dmr_file_info", default="Fragments_MseI_TTAA_100-350.txt",help="Fragments_MseI_TTAA_100-350.txt", metavar="SAMP")

#(options, args) = parser.parse_args()
                  
#infile=options.dmr_file_info

#dm_info=options.dm_info
#dm_info=str(dm_info)
#---------------------------------------------------------------
genome_path="../shelve/"
refgene_file="maize_AGPv3_enhanced_refgenes.shelve"
d1 = shelve.open(genome_path+refgene_file,'r')
# Promoters "P","p" (1000bp upstream)
# Exons "E","e"
# Introns "I","i"
# Intergentic "-"
# Splice site "S"
# UTR "5","3"

#print d1.keys()

cpg_file="maize_AGPv3_repeats.shelve"
d2 = shelve.open(genome_path+cpg_file,'r')
# CpG islands "I
# else: "-"
#print d2.keys()

chrs=d1.keys()
chrs.sort()

#---------------------------------------------------------------
frag_path="/work1/home/paoyang/projects/maize/RRBS_enrichment/fragments/"


n_all=0
n_promoter=0
n_exon=0
n_intron=0
n_inter=0
n_five=0
n_three=0
n_splice=0
n_repeat=0

for c in chrs:
	seq1=d1[c]
	seq2=d2[c]
	for i in range(len(seq1)):
		x1=seq1[i].upper()
		x2=seq2[i].upper()
		n_all+=1
		if x1=="-":
			n_inter+=1
		elif x1=="P":
			n_promoter+=1
		elif x1=="E":
			n_exon+=1
		elif x1=="I":
			n_intron+=1
		elif x1=="5":
			n_five+=1
		elif x1=="3":
			n_three+=1
		elif x1=="S":
			n_splice+=1
		if x2=="I":
			n_repeat+=1

f_all=float(n_all)/n_all
f_promoter=float(n_promoter)/n_all
f_exon=float(n_exon)/n_all
f_intron=float(n_intron)/n_all
f_inter=float(n_inter)/n_all
f_five=float(n_five)/n_all
f_three=float(n_three)/n_all
f_splice=float(n_splice)/n_all
f_repeat=float(n_repeat)/n_all

for file in os.listdir(frag_path):
	if file.startswith("Fragments_") and "4" in file:
		infile=file	

		outfile="enrichment-"+infile
		outf=open(outfile,"w")

		outf.write("\tALL\tPromoter\t5UTR\tExon\tIntron\t3UTR\tSplice_site\tRepeats\tIntergenic"+"\n")		
		outf.write("Genome\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d"%(n_all,n_promoter,n_five,n_exon,n_intron,n_three,n_splice,n_repeat,n_inter)+"\n")		
		outf.write("Genome(percentage)\t%1.3f\t%1.3f\t%1.3f\t%1.3f\t%1.3f\t%1.3f\t%1.3f\t%1.3f\t%1.3f"\
		%(f_all,f_promoter,f_five,f_exon,f_intron,f_three,f_splice,f_repeat,f_inter)+"\n")		

		#---------------------------------------------------------------

		frag_dic={}

		for line in fileinput.input(frag_path+infile):
			if line[0]!="#":
				l=line.split()
				chr=str(l[0])
				s1=int(l[1])
				s2=int(l[2])
				if chr in frag_dic:
					frag_dic[chr].append([s1,s2])
				else:
					frag_dic[chr]=[[s1,s2]]
		fileinput.close()			

		#---------------------------------------------------------------

		nc_all=0
		nc_promoter=0
		nc_exon=0
		nc_intron=0
		nc_inter=0
		nc_five=0
		nc_three=0
		nc_splice=0
		nc_repeat=0

		chrs=frag_dic.keys()
		for c in chrs:
			seq1=d1[c].upper()
			seq2=d2[c].upper()
			frag_lst=frag_dic[c]
			for my_frag in frag_lst:
				t1=my_frag[0]
				t2=my_frag[1]
				for i in range(t1,t2):
					nc_all+=1
					x1=seq1[i]
					x2=seq2[i]
					if x1=="-":
						nc_inter+=1
					elif x1=="P":
						nc_promoter+=1
					elif x1=="E":
						nc_exon+=1
					elif x1=="I":
						nc_intron+=1
					elif x1=="5":
						nc_five+=1
					elif x1=="3":
						nc_three+=1
					elif x1=="S":
						nc_splice+=1
					if x2=="I":
						nc_repeat+=1
						
		ff_all=float(nc_all)/nc_all
		ff_promoter=float(nc_promoter)/nc_all
		ff_exon=float(nc_exon)/nc_all
		ff_intron=float(nc_intron)/nc_all
		ff_inter=float(nc_inter)/nc_all
		ff_five=float(nc_five)/nc_all
		ff_three=float(nc_three)/nc_all
		ff_splice=float(nc_splice)/nc_all
		ff_repeat=float(nc_repeat)/nc_all

		fc_all=math.log(float(ff_all)/f_all,2)
		fc_promoter=math.log(float(ff_promoter)/f_promoter,2)
		fc_exon=math.log(float(ff_exon)/f_exon,2)
		fc_intron=math.log(float(ff_intron)/f_intron,2)
		fc_inter=math.log(float(ff_inter)/f_inter,2)
		fc_five=math.log(float(ff_five)/f_five,2)
		fc_three=math.log(float(ff_three)/f_three,2)
		fc_splice=math.log(float(ff_splice)/f_splice,2)
		fc_repeat=math.log(float(ff_repeat)/f_repeat,2)

		outf.write("RRBS\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d"%(nc_all,nc_promoter,nc_five,nc_exon,nc_intron,nc_three,nc_splice,nc_repeat,nc_inter)+"\n")		
		outf.write("RRBS(percentage)\t%1.3f\t%1.3f\t%1.3f\t%1.3f\t%1.3f\t%1.3f\t%1.3f\t%1.3f\t%1.3f"\
		%(ff_all,ff_promoter,ff_five,ff_exon,ff_intron,ff_three,ff_splice,ff_repeat,ff_inter)+"\n")
		outf.write("Log2FC\t%1.3f\t%1.3f\t%1.3f\t%1.3f\t%1.3f\t%1.3f\t%1.3f\t%1.3f\t%1.3f"\
		%(fc_all,fc_promoter,fc_five,fc_exon,fc_intron,fc_three,fc_splice,fc_repeat,fc_inter)+"\n")



d1.close()			
d2.close()			

outf.close()
print "End -- %s"%(infile);