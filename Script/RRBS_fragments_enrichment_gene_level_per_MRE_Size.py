﻿from os import listdir
import fileinput, string,os, operator, shelve, time, subprocess, math
from subprocess import Popen
import operator
from optparse import OptionParser
parser = OptionParser()

parser.add_option("-i", "--initial", type="string",dest="MRE_group", default="MSEI")
parser.add_option("-s", "--size", type="string",dest="Size_info", default="40-300")
parser.add_option("-c", "--overlapc", type="int",dest="Overlap_info", default=1)

(options, args) = parser.parse_args()
                  
xx0=options.MRE_group
xx0=xx0.upper()
xx="Fragments_"+xx0

size_range=options.Size_info

z=options.Overlap_info
#dm_info=options.dm_info
#dm_info=str(dm_info)
#---------------------------------------------------------------

def counting_g(gene_lst,i,frag_sequences):
	glst=[x[i] for x in gene_lst]
	ng=len(glst)
	k=0
	for g in glst:
		x=range(g[0],g[1])
		f_counts=frag_sequences.count("C",g[0],g[1])
		if f_counts>= z: #<=== at least 1 C
			k+=1
	return ng,k;
			
		
	


#---------------------------------------------------------------
genome_path="../shelve/"
chr_d={}
refgene_file="maize_AGPv3_enhanced_refgenes.shelve"
d1 = shelve.open(genome_path+refgene_file,'r')
# Promoters "P","p" (1000bp upstream)
# Exons "E","e"
# Introns "I","i"
# Intergentic "-"
# Splice site "S"
# UTR "5","3"

#print d1.keys()
chrs=d1.keys()
chrs.sort()

for c in chrs:
	chr_d[c]=len(d1[c])

Genome_size=0
for c in chrs:
	Genome_size+=len(d1[c])
d1.close()

xyz={}
xyz_file="maize_AGPv3-CG-CHG-CHH.shelve"
xyz = shelve.open(genome_path+xyz_file,'r')



file="uniq-38653-maize_Ensembl_AGPv3_refFlat.txt"


gene_map={}
n=0
for line in fileinput.input("./"+file):
	l=line.split()
	mychr=str(l[0])
	gene=str(l[1])
	strand=str(l[3])
	t1=int(l[4])
	t2=int(l[5])
	genebody=[t1,t2]
	if strand == "+":
		pmt=[t1-1000,t1]
	else:
		pmt=[t2,t2+1000]
	n+=1
	if mychr in gene_map:
		gene_map[mychr].append([genebody,pmt])
	else:
		gene_map[mychr]=[[genebody,pmt]]
fileinput.close()


#---------------------------------------------------------------
frag_path="/work1/home/paoyang/projects/maize/RRBS_enrichment/fragments/"

for file in os.listdir(frag_path):
	frag_size=0
	n_gene=0
	n_genebody=0
	n_pmt=0
	if file.startswith("Fragments_") and size_range in file and xx.upper() in file.upper():
		infile=file
		enzyme_name=file.split("_")[1]
		cutting_site=file.split("_")[2]
		Size_Selection=str(file.split("_")[3][:-4])


		#---------------------------------------------------------------

		frag_dic={}

		for line in fileinput.input(frag_path+infile):
			if line[0]!="#":
				l=line.split()
				chr=str(l[0])
				s1=int(l[1])
				s2=int(l[2])
				d=int(l[3])
				frag_size+=d
				if chr in frag_dic:
					frag_dic[chr].append([s1,s2])
				else:
					frag_dic[chr]=[[s1,s2]]
		fileinput.close()			
		chrs=frag_dic.keys()
		chrs.sort()
		
		#---------------------------------------------------------------
		frag_seqs={}
		for c in chrs:
			L=chr_d[c]
			lst=["-" for x in range(L)]
			fraglst=frag_dic[c]
			xyzseq=xyz[c]
			
			for f in fraglst:
				for i in range(f[0],f[1]):
					if xyzseq[i] !="-":
						lst[i]="C"
			frag_seqs[c]="".join(lst)
			lst=[]

		#---------------------------------------------------------------
		sn0=0
		sn1=0
		sn2=0
		sn3=0
		
		for c in chrs:
			gene_lst=gene_map[c]
			frag_sequences=frag_seqs[c]
			n0,n1=counting_g(gene_lst,0,frag_sequences)
			n2,n3=counting_g(gene_lst,1,frag_sequences)
			
			sn0+=n0
			sn1+=n1
			sn2+=n2
			sn3+=n3
			
		#---------------------------------------------------------------
								
		f_Fragment_Covered	= float(frag_size)/Genome_size
		f_Covered_Genebody	= float(sn1)/n
		f_Covered_Pmt		= float(sn3)/n

		e_Covered_Genebody	= math.log(float(f_Covered_Genebody)/f_Fragment_Covered,2)
		e_Covered_Pmt		= math.log(float(f_Covered_Pmt)/f_Fragment_Covered,2)

		#---------------------------------------------------------------
		
		

		outfile="Fragment-Enrichment-genebody-pmt-"+xx0+"-"+size_range+"-"+str(z)+".txt"
		outf=open(outfile,"w")
		outf.write("Enzyme\tCutting_Site\tSize_Selection\tGenome_Size\tFragment_Covered\tFragment_Covered_Pct\tTotal_Genes\tCovered_Genebody\tCovered_Genebody_Pct\tLog2_Fold_Enrichment_Genebody\tCovered_Pmt\tCovered_Pmt_Pct\tLog2_Fold_Enrichment_Pmt"+"\n")		
		

		outf.write("%s\t%s\t%s\t%d\t%d\t%1.3f\t%d\t%d\t%1.3f\t%1.3f\t%d\t%1.3f\t%1.3f"\
		%(enzyme_name,cutting_site,Size_Selection,Genome_size,frag_size,f_Fragment_Covered,\
		n,sn1,f_Covered_Genebody,e_Covered_Genebody,sn3,f_Covered_Pmt,e_Covered_Pmt)+"\n")		
		outf.close()
		
		print "End -- %s"%(infile)
		
xyz.close()
print "End";